<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Pictogram\BaseControl;
use Skadmin\Pictogram\Doctrine\Pictogram\Pictogram;
use Skadmin\Pictogram\Doctrine\Pictogram\PictogramFacade;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory       $webLoader;
    private PictogramFacade     $facade;
    private PictogramTypeFacade $facadePictogramType;
    private Pictogram           $pictogram;
    private ImageStorage        $imageStorage;

    public function __construct(?int $id, PictogramFacade $facade, PictogramTypeFacade $facadePictogramType, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade              = $facade;
        $this->facadePictogramType = $facadePictogramType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->pictogram = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->pictogram->isLoaded()) {
            return new SimpleTranslation('pictogram.edit.title - %s', $this->pictogram->getName());
        }

        return 'pictogram.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->pictogram->isLoaded()) {
            if ($identifier !== null && $this->pictogram->getImagePreview() !== null) {
                $this->imageStorage->delete($this->pictogram->getImagePreview());
            }

            $pictogram = $this->facade->update(
                $this->pictogram->getId(),
                $values->name,
                $values->isActive,
                $this->facadePictogramType->get($values->pictogramTypeId),
                $identifier
            );
            $this->onFlashmessage('form.pictogram.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $pictogram = $this->facade->create(
                $values->name,
                $values->isActive,
                $this->facadePictogramType->get($values->pictogramTypeId),
                $identifier
            );
            $this->onFlashmessage('form.pictogram.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $pictogram->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->pictogram = $this->pictogram;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataPictogramsType = $this->facadePictogramType->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.pictogram.edit.name')
            ->setRequired('form.pictogram.edit.name.req');
        $form->addCheckbox('isActive', 'form.pictogram.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('imagePreview', 'form.pictogram.edit.image-preview');

        $form->addSelect('pictogramTypeId', 'form.pictogram.edit.pictogram-type-id', $dataPictogramsType)
            ->setRequired('form.pictogram.edit.pictogram-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.pictogram.edit.send');
        $form->addSubmit('sendBack', 'form.pictogram.edit.send-back');
        $form->addSubmit('back', 'form.pictogram.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->pictogram->isLoaded()) {
            return [];
        }

        return [
            'name'            => $this->pictogram->getName(),
            'isActive'        => $this->pictogram->isActive(),
            'pictogramTypeId' => $this->pictogram->getPictogramType()->getId(),
        ];
    }
}

<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Pictogram\BaseControl;
use Skadmin\Pictogram\Doctrine\Pictogram\Pictogram;
use Skadmin\Pictogram\Doctrine\Pictogram\PictogramFacade;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private PictogramFacade     $facade;
    private PictogramTypeFacade $facadePictogramType;
    private LoaderFactory       $webLoader;
    private ImageStorage        $imageStorage;

    public function __construct(PictogramFacade $facade, PictogramTypeFacade $facadePictogramType, Translator $translator, User $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade              = $facade;
        $this->facadePictogramType = $facadePictogramType;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'pictogram.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataPictogramsType = $this->facadePictogramType->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->leftJoin('a.pictogramType', 'b')
            ->orderBy('b.sequence', 'ASC')
            ->addOrderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Pictogram $pictogram): ?Html {
                if ($pictogram->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$pictogram->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.pictogram.overview.name')
            ->setRenderer(function (Pictogram $pictogram): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $pictogram->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($pictogram->getName());

                return $name;
            });
        $grid->addColumnText('pictogramType', 'grid.pictogram.overview.pictogram-type', 'pictogramType.name');
        $this->addColumnIsActive($grid, 'pictogram.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.pictogram.overview.name', ['name']);
        $grid->addFilterSelect('pictogramType', 'grid.pictogram.overview.pictogram-type', $dataPictogramsType, 'pictogramType')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'pictogram.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.pictogram.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.pictogram.overview.action.pictogram-type', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.pictogram.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // OTHER
        $grid->setDefaultFilter(['isActive' => 1]);

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $pictogram = $this->facade->get(intval($itemId));
        $this->facade->sort($itemId, $prevId, $nextId, ['pictogram_type_id = %d' => $pictogram->getPictogramType()->getId()]);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.pictogram.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}

<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}

<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramType;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Pictogram\BaseControl;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewType extends GridControl
{
    use APackageControl;

    private PictogramTypeFacade $facade;
    private LoaderFactory       $webLoader;

    public function __construct(PictogramTypeFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;

        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewType.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'pictogram.overview-type.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelSorted(sortProperty: 'a.sequence'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.pictogram.overview-type.name');

        // FILTER
        $grid->addFilterText('name', 'grid.pictogram.overview-type.name', ['name', 'code']);

        // ACTION

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.pictogram.overview-type.action.pictogram', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('icons')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.pictogram.overview-type.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.pictogram.overview-type.name')
            ->setRequired('grid.pictogram.overview-type.name.req');
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $pictogramType = $this->facade->create($values->name);

        $message = new SimpleTranslation('grid.pictogram.overview-type.action.flash.inline-add.success "%s"', [$pictogramType->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $this->onInlineAdd($container);
    }

    public function onInlineEditDefaults(Container $container, PictogramType $pictogramType): void
    {
        $container->setDefaults([
            'name' => $pictogramType->getName(),
        ]);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit(int|string $id, ArrayHash $values): void
    {
        $pictogramType = $this->facade->update(intval($id), $values->name);

        $message = new SimpleTranslation('grid.pictogram.overview-type.action.flash.inline-edit.success "%s"', [$pictogramType->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.pictogram.overview-type.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}

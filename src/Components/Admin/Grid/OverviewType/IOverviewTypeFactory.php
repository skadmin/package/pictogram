<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}

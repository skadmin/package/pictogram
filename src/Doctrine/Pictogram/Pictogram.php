<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Doctrine\Pictogram;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramType;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Pictogram
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\ManyToOne(targetEntity: PictogramType::class, inversedBy: 'pictograms')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private PictogramType $pictogramType;

    public function update(string $name, bool $isActive, PictogramType $pictogramType, ?string $imagePreview): void
    {
        $this->name          = $name;
        $this->pictogramType = $pictogramType;

        $this->setIsActive($isActive);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getPictogramType(): PictogramType
    {
        return $this->pictogramType;
    }
}

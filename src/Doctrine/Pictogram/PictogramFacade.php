<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Doctrine\Pictogram;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Pictogram\Doctrine\PictogramType\PictogramType;
use SkadminUtils\DoctrineTraits;
use SkadminUtils\DoctrineTraits\Facade;

final class PictogramFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Pictogram::class;
    }

    public function create(string $name, bool $isActive, PictogramType $pictogramType, ?string $imagePreview): Pictogram
    {
        return $this->update(null, $name, $isActive, $pictogramType, $imagePreview);
    }

    public function update(?int $id, string $name, bool $isActive, PictogramType $pictogramType, ?string $imagePreview): Pictogram
    {
        $pictogram = $this->get($id);
        $pictogram->update($name, $isActive, $pictogramType, $imagePreview);

        if (! $pictogram->isLoaded()) {
            $pictogram->setSequence($this->getValidSequence());
        }

        $this->em->persist($pictogram);
        $this->em->flush();

        return $pictogram;
    }

    public function get(?int $id = null): Pictogram
    {
        if ($id === null) {
            return new Pictogram();
        }

        $pictogram = parent::get($id);

        if ($pictogram === null) {
            return new Pictogram();
        }

        return $pictogram;
    }

    /**
     * @return Pictogram[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?Pictogram
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}

<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Doctrine\PictogramType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Pictogram\Doctrine\Pictogram\Pictogram;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
class PictogramType
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Code;
    use Entity\Sequence;

    /** @var ArrayCollection|Collection|Pictogram[] */
    #[ORM\OneToMany(targetEntity: Pictogram::class, mappedBy: 'pictogramType')]
    private ArrayCollection|Collection|array $pictograms;

    public function __construct()
    {
        $this->pictograms = new ArrayCollection();
    }

    public function update(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection|Collection|Pictogram[]
     */
    public function getPictograms(bool $onlyActive = false): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        $criteria->orderBy(['sequence' => 'ASC']);

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->pictograms->matching($criteria);
    }
}

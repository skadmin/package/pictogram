<?php

declare(strict_types=1);

namespace Skadmin\Pictogram\Doctrine\PictogramType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class PictogramTypeFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = PictogramType::class;
    }

    public function create(string $name): PictogramType
    {
        return $this->update(null, $name);
    }

    public function update(?int $id, string $name): PictogramType
    {
        $pictogramType = $this->get($id);
        $pictogramType->update($name);

        if (! $pictogramType->isLoaded()) {
            $pictogramType->setSequence($this->getValidSequence());
        }

        $this->em->persist($pictogramType);
        $this->em->flush();

        return $pictogramType;
    }

    public function get(?int $id = null): PictogramType
    {
        if ($id === null) {
            return new PictogramType();
        }

        $pictogramType = parent::get($id);

        if ($pictogramType === null) {
            return new PictogramType();
        }

        return $pictogramType;
    }

    /**
     * @return PictogramType[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findBy([], ['sequence' => 'ASC']);
    }

    public function findByCode(string $code): ?PictogramType
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}

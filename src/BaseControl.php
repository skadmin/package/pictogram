<?php

declare(strict_types=1);

namespace Skadmin\Pictogram;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'pictogram';
    public const DIR_IMAGE = 'pictogram';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-icons']),
            'items'   => ['overview'],
        ]);
    }
}

<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220820115641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pictogram (id INT AUTO_INCREMENT NOT NULL, pictogram_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT 1 NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, INDEX IDX_56E0A15F7DC975B7 (pictogram_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pictogram_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pictogram ADD CONSTRAINT FK_56E0A15F7DC975B7 FOREIGN KEY (pictogram_type_id) REFERENCES pictogram_type (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pictogram DROP FOREIGN KEY FK_56E0A15F7DC975B7');
        $this->addSql('DROP TABLE pictogram');
        $this->addSql('DROP TABLE pictogram_type');
    }
}
